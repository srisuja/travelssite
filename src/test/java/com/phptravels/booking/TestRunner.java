package com.phptravels.booking;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:Feature"
        , format = { "pretty", "html:target/site/cucumber-reports"
        , "json:target/cucumber.json"}
        , tags = "@Runall"
)


public class TestRunner {


}
