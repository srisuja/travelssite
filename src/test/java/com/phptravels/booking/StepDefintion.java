package com.phptravels.booking;

import com.phptravels.pages.AvailabilityofRooms;
import com.phptravels.pages.Loginpage;
import com.phptravels.pages.PaymentandConformation;
import com.phptravels.pages.SearchingforHotels;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.List;

import java.util.concurrent.TimeUnit;


public class StepDefintion {
    WebDriver driver = null;
    WebElement Element = null;
    WebElement Country_Dropdown = null;
    Alert alert = null;
    RequestSpecification request = RestAssured.given();
    Response response=null;

    @Given("^I navigate to login page$")
    public void i_navigate_to_login_page() throws Throwable {

        System.setProperty("webdriver.chrome.driver", "C:/Users/giris/Desktop/selenium/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.phptravels.net/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @When("^I enter username as \"([^\"]*)\" in Email Address$")
    public void i_enter_username_as_in_Email_Address(String arg1) throws Throwable {
        Loginpage signin = new Loginpage(driver);
        signin.clickOn_MyAccount();
        signin.clickOn_Login();
        signin.enter_EmailAddress(arg1);

    }

    @And("^I enter password as \"([^\"]*)\" in Password$")
    public void i_enter_password_as_in_Password(String arg1) throws Throwable {
        Loginpage signin = new Loginpage(driver);
        signin.enter_Password(arg1);
    }

    @And("^I click on Login button$")
    public void i_click_on_Login_button() throws Throwable {
        Loginpage signin = new Loginpage(driver);
        signin.clickOn_Loginbtn();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @Given("^I Search for hotels in \"([^\"]*)\"$")
    public void i_Search_for_hotels_in(String arg1) throws Throwable {
        SearchingforHotels hotel = new SearchingforHotels(driver);
        hotel.clickOn_HotelsLink();
        hotel.select_City("Singapore");
        hotel.select_checkin();
        hotel.select_Numberofperson();
        hotel.searching_Hotels();

    }

    @Given("^I check for availablity of room and confirm the order$")
    public void i_check_for_availablity_of_room_and_confirm_the_order() throws Throwable {
        AvailabilityofRooms availroom = new AvailabilityofRooms(driver);
        availroom.scroll_AvailableRoom();
        availroom.confirm_RoomBooking();
        availroom.clickOn_ConfirmthisBooking();

    }

    @Then("^I choose the payment option for completing the reservation process$")
    public void i_choose_the_payment_option_for_completing_the_reservation_process() throws Throwable {
        PaymentandConformation payment = new PaymentandConformation(driver);
        payment.select_PaymentOption();
        driver.switchTo().alert().accept();
        String confirmation_Message = payment.getconfirmationmsg();
        Assert.assertEquals("RESERVED", confirmation_Message);
    }

    @When("^I enters \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_enters_and(String arg1, String arg2) throws Throwable {
        Loginpage signin = new Loginpage(driver);
        signin.clickOn_MyAccount();
        signin.clickOn_Login();
        signin.enter_EmailAddress(arg1);
        signin.enter_Password(arg2);
        signin.clickOn_Loginbtn();
    }

    @Then("^Login is sucessfull $")
    public void Login_is_sucessfull() throws Throwable {
        Loginpage signin = new Loginpage(driver);
        signin.clickOn_username();
        boolean value=signin.displayed_logoutbtn();
        Assert.assertEquals("True", value);
        signin.clickon_logoutbtn();
    }

    @Then("^Capture the error message $")
    public void Capture_the_error_message() throws Throwable {
        Loginpage signin = new Loginpage(driver);
        boolean value=signin.verify_logouterrormsg();
        Assert.assertEquals("True", value);

    }
    @Given("^user set a header and body parameters to post\\.$")
    public void user_set_a_header_and_body_parameters_to_post() throws Throwable {
        String jsonbody="{\n" +
                "\"power\": \"1\"\n" +
                "}";

        request.header("userid", "1d15e600-a226-c673-6e83-baf59fb8e966");
        request.header("Content-Type", "application/json");
        request.body(jsonbody);

    }

    @When("^user post a request to endpoint \"([^\"]*)\"\\.$")
    public void user_post_a_request_to_endpoint(String arg1) throws Throwable {
        Response response = request.post(arg1);
        int status = response.getStatusCode();
        Assert.assertEquals(200, status);
    }
    @Then("^verify the status msg$")
    public void verify_the_status_msg() throws Throwable {

    }
}

















