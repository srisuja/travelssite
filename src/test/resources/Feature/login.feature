Feature: Login functionality for a PHPtravels site.

@Multipleuser
  Scenario: Successful Login with valid credentials
    Given I navigate to login page
    When I enter username as "testing10@gmail.com" in Email Address
    And I enter password as "test123" in Password
    And Login is sucessfull
@Successfullogin
  Scenario Outline: Successful Login with Multiple Valid Credentials
    Given I navigate to login page
    When I enters "<username>" and "<password>"
    Then Login is sucessfull
    Examples:
      | username   | password |
      | test1@gmail.com | test123 |
      | test2@gmail.com | test123 |
@Unsuccessfullogin
  Scenario: Login with invalid credentials
    Given I navigate to login page
    When I enter username as "testing10@gmail.com" in Email Address
    And I enter password as "test123" in Password
    And Capture the error message
