Feature: Login functionality for a PHPtravels site.
  The user should be able to login into the PHPtravels site if the username and the password are correct

@Hotelbooking
  Scenario: Successful scenario for Hotel booking
    Given I navigate to login page
    When I enter username as "testing5@gmail.com" in Email Address
    And I enter password as "test123" in Password
    And I click on Login button
    And  I Search for hotels in "Singapore"
    And  I check for availability of room and confirm the order
    Then  I choose the payment option for completing the reservation process







