package com.phptravels.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SearchingforHotels {
    WebDriver driver;

    public SearchingforHotels(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using= "//a[contains(text( ),'Hotels')]")
    private WebElement link_Hotels;

    @FindBy(how = How.CLASS_NAME, using="select2-choice")
    private WebElement txtbx_City;

    @FindBy(how = How.XPATH, using= "//input[@class='select2-input select2-focused']")
    private WebElement txtbx_CityName;

    @FindBy(how = How.XPATH, using= "//div[@id='select2-drop']//ul[@class='select2-results']/li[div[text()='Hotels']]//li")
    private WebElement txtbx_SelectCityName;

    @FindBy(how = How.XPATH, using= "//input[@name='checkin']")
    private WebElement cal_Checkin;

    @FindBy(how = How.XPATH, using= "//div[@class=\"datepicker-days\"]//tr[3]/td[2]")
    private WebElement cal_SelectCheckin;

    @FindBy(how = How.ID, using="travellersInput")
    private WebElement txtbx_Numberofpeople;

    @FindBy(how = How.ID, using="adultPlusBtn")
    private WebElement txtbx_selectNumberofpeople;

    @FindBy(how = How.XPATH, using= "//button[@class='btn btn-lg btn-block btn-danger pfb0 loader']")
    private WebElement btn_Search;

    public void clickOn_HotelsLink() {
        WebDriverWait wait=new WebDriverWait(driver, 20);
        WebElement link=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text( ),'Hotels')]")));
        link.click();
    }

    public void select_City(String str1) {
        txtbx_City.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        txtbx_CityName.sendKeys(str1);
        txtbx_SelectCityName.click();

    }

    public void select_checkin() {
        cal_Checkin.click();
        cal_SelectCheckin.click();

    }
    public void select_Numberofperson() {
        txtbx_Numberofpeople.click();
        txtbx_selectNumberofpeople.click();

    }

    public void searching_Hotels() {
        btn_Search.click();


    }



}
