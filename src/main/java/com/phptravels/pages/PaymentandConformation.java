package com.phptravels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PaymentandConformation {
    WebDriver driver;

    public PaymentandConformation(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using="//button[contains(text(),'Pay on Arrival')]")
    private WebElement btn_PayOnArrival;

    @FindBy(how = How.XPATH, using="//table[@id='invoiceTable']/tbody/tr[1]/td/div/b")
    private WebElement message;


    public void select_PaymentOption() {
        btn_PayOnArrival.click();
    }

    public String getconfirmationmsg() {

       return message.getText();
    }




}
