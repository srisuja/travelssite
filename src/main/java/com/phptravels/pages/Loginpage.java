package com.phptravels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Loginpage {
    WebDriver driver;

    public Loginpage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//nav[@class='navbar navbar-default'] //li[@id='li_myaccount']/a")
    private WebElement link_MyAccount;

    @FindBy(how = How.XPATH, using = "//nav[@class='navbar navbar-default'] //li[@id='li_myaccount']/ul/li[1]/a")
    private WebElement link_Login;

    @FindBy(how = How.NAME, using = "username")
    private WebElement txtbx_EmailAddress;

    @FindBy(how = How.NAME, using = "password")
    private WebElement txtbx_Password;

    @FindBy(how = How.XPATH, using = "//button[contains(text( ),'Login')]")
    private WebElement btn_Login;

    @FindBy(how = How.XPATH, using = "//div[@id='collapse']//li[1]/a[@class='dropdown-toggle go-text-right']")
    private WebElement link_username;

    @FindBy(how = How.XPATH, using = "//div[@id='collapse']/ul[2]/ul/li[1]/ul/li[2]/a")
    private WebElement link_logout;

    @FindBy(how = How.XPATH, using = "//div[@class='alert alert-danger']")
    private WebElement logout_errormsg;



    public void clickOn_MyAccount() {
        link_MyAccount.click();
    }

    public void clickOn_Login() {
        link_Login.click();
    }

    public void enter_EmailAddress(String str1) {
        txtbx_EmailAddress.sendKeys(str1);
    }

    public void enter_Password(String str1) {
        txtbx_Password.sendKeys(str1);
    }

    public void clickOn_Loginbtn() {
        btn_Login.click();
    }

    public void clickOn_username() {
        link_username.click();
    }

    public boolean displayed_logoutbtn() {
        return link_logout.isDisplayed();
    }

    public void clickon_logoutbtn() {
        link_logout.click();
    }

    public boolean verify_logouterrormsg() {
        return logout_errormsg.isDisplayed();
    }






}

