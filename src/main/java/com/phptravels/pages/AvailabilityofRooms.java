package com.phptravels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static sun.plugin.javascript.navig.JSType.Element;

public class AvailabilityofRooms {

    WebDriver driver;
    WebElement Element = null;

    public AvailabilityofRooms(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using= "//div[contains(text(),'Available Rooms')]")
    private WebElement txt_AvailableRooms;

    @FindBy(how = How.XPATH, using= "//section[@id='ROOMS']//table//tr[1]//button[contains(text(),'Book Now')]")
    private WebElement btn_BookNow;

    @FindBy(how = How.NAME, using="logged")
    private WebElement btn_ConfirmthisBooking;



    public void scroll_AvailableRoom() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Element = txt_AvailableRooms;
        js.executeScript("arguments[0].scrollIntoView();", Element);
    }

    public void confirm_RoomBooking() {
        btn_BookNow.click();
    }
    public void clickOn_ConfirmthisBooking() {
        btn_ConfirmthisBooking.click();
    }


}
